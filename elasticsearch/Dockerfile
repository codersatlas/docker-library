FROM adoptopenjdk/openjdk12:x86_64-alpine-jdk-12.0.2_10

ENV VERSION 7.3.0
ENV TINYLOGV 2.0.0-RC2
ENV DOWNLOAD_URL "https://artifacts.elastic.co/downloads/elasticsearch"
ENV ES_TARBAL "${DOWNLOAD_URL}/elasticsearch-${VERSION}-no-jdk-linux-x86_64.tar.gz"

COPY elasticsearch/es-entrypoint.sh /

RUN apk -U upgrade --no-cache \
    && jlink --no-header-files --no-man-pages -G --vm=server --dedup-legal-notices=error-if-not-same-content --compress=2 --add-modules java.management,jdk.unsupported,java.xml,java.naming,java.base,java.sql --output /usr/share/jre \
    && rm -rf /usr/share/jre/conf/security/policy/README.txt /usr/share/jre/release /usr/share/jre/lib/server/Xusage.txt /usr/share/jre/bin/javaw \
    && apk add --no-cache su-exec haveged fontconfig bash \
    && apk add --no-cache -t .build-deps wget ca-certificates openssl \
    && rm -rf /opt/java \
    && /usr/share/jre/bin/java -Xshare:dump \
    && set -ex \
    && cd /tmp \
    && chmod +x /es-entrypoint.sh \
    && wget -q -O elasticsearch.tar.gz "$ES_TARBAL"; \
    tar -xf elasticsearch.tar.gz \
    && ls -lah \
    && mv elasticsearch-$VERSION /usr/share/elasticsearch \
    && rm -rf /usr/share/elasticsearch/bin/elasticsearch-sql-* /usr/share/elasticsearch/lib/tools /usr/share/elasticsearch/config/log4j2.properties /usr/share/elasticsearch/modules/x-pack-voting-only-node /usr/share/elasticsearch/modules/lang-mustache /usr/share/elasticsearch/modules/x-pack-deprecation /usr/share/elasticsearch/modules/x-pack-sql \
    && wget -q -O /usr/share/elasticsearch/lib/log4j-to-slf4j-2.12.1.jar https://maven-central.storage-download.googleapis.com/repos/central/data/org/apache/logging/log4j/log4j-to-slf4j/2.12.1/log4j-to-slf4j-2.12.1.jar \
    && wget -q -O /usr/share/elasticsearch/lib/slf4j-tinylog-${TINYLOGV}.jar https://maven-central.storage-download.googleapis.com/repos/central/data/org/tinylog/slf4j-tinylog/${TINYLOGV}/slf4j-tinylog-${TINYLOGV}.jar \
    && wget -q -O /usr/share/elasticsearch/lib/tinylog-impl-${TINYLOGV}.jar https://maven-central.storage-download.googleapis.com/repos/central/data/org/tinylog/tinylog-impl/${TINYLOGV}/tinylog-impl-${TINYLOGV}.jar \
    && adduser -D -h /usr/share/elasticsearch elasticsearch \
    && for path in \
    /usr/share/elasticsearch/data \
    /usr/share/elasticsearch/logs \
    /usr/share/elasticsearch/config \
    /usr/share/elasticsearch/config/scripts \
    /usr/share/elasticsearch/tmp \
    /usr/share/elasticsearch/plugins \
    ; do \
    mkdir -p "$path"; \
    chown -R elasticsearch:elasticsearch "$path"; \
    done \
    && rm -rf /tmp/* \
    && apk del --purge .build-deps

ENV JAVA_HOME=/usr/share/jre
ENV PATH=${PATH}:/usr/share/jre/bin

COPY elasticsearch/config/elastic /usr/share/elasticsearch/config

WORKDIR /usr/share/elasticsearch

ENV PATH /usr/share/elasticsearch/bin:$PATH
ENV ES_TMPDIR /usr/share/elasticsearch/tmp

VOLUME ["/usr/share/elasticsearch/data"]

EXPOSE 9200 9300
ENTRYPOINT ["/es-entrypoint.sh"]
CMD ["elasticsearch"]