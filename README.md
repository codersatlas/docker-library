# docker-library
Collection of docker images   

[![pipeline status](https://gitlab.com/codersatlas/docker-library/badges/master/pipeline.svg)](https://gitlab.com/codersatlas/docker-library/commits/master)
[![coverage report](https://gitlab.com/codersatlas/docker-library/badges/master/coverage.svg)](https://gitlab.com/codersatlas/docker-library/commits/master)

# Usage

- Setup new server with with Ubuntu minimal  
- Install docker containers as required  

OR

Use in CI/CD pipelines