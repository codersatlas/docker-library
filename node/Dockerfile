FROM registry.gitlab.com/codersatlas/docker-library/ubuntu:latest

ENV NODE_VERSION 12.8.0

RUN groupadd --gid 1000 node \
    && useradd --uid 1000 --gid node --shell /bin/bash --create-home node \
    && buildDeps='xz-utils ca-certificates curl' \
    && apt-get update -y && apt-get install -y $buildDeps --no-install-recommends \
    && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz" \
    && tar -xJf "node-v$NODE_VERSION-linux-x64.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
    && apt-get purge -y --auto-remove $buildDeps \
    && apt-get clean -y \
    && rm -rf "node-v$NODE_VERSION-linux-x64.tar.xz" /var/lib/apt/lists/* /var/log/* /tmp/* \
    && npm i -g npm

COPY node/entrypoint.sh /usr/local/bin/
ENTRYPOINT ["entrypoint.sh"]

CMD [ "node" ]