#!/bin/bash
set -euo pipefail
eval JAVA_OPTS=\"${JAVA_OPTS}\"

export CLASSPATH="${CLASSPATH_DEFAULT}"

if [ -n "${JAVA_OPTS}" ]; then
  export JAVA_OPTS="${JAVA_OPTS_DEFAULT} ${JAVA_OPTS}"
else
  export JAVA_OPTS="${JAVA_OPTS_DEFAULT}"
fi
set -x
exec /usr/share/jre/bin/java -server ${JAVA_OPTS} com.hazelcast.core.server.StartServer